# ost splitter

python3 program that splits up mp3 files containing multiple songs

**python3 audiosplit.py mp3_file time_stamp_file [-n name_file] [-t tags_file]**



| arg | desc |
| ------ | ------ |
| time_stamp_file | lists HH:MM:SS that each song ENDS |
| name_file  | lists each songs name |
| tags_file | lists tags in the format "tag":"value" |