from pydub import *
import sys, getopt

def getTags(file_name):
    tags = {}
    with open(file_name) as rp:
        for x in rp:
            line = x.strip()
            line_list = line.split(":")
            tags[line_list[0]] = line_list[1]
    return tags
            
def toMili(base_str):
    if(type(base_str) != str): raise Exception("Invalid base_str")
    time_list = base_str.split(":")
    if(len(time_list) == 0): raise Exception("base_str not split 2")

    total = int(time_list[-1])
    if( len(time_list) > 1): total += int(time_list[-2]) * 60
    if( len(time_list) > 2): total += int(time_list[-3]) * 60 * 60
    
    #minutes = int(time_list[0])
    #seconds = int(time_list[1])

    return total * 1000
    
#TIMES = ["0:50", "1:20", "2:00", "2:45"]
#if len(sys.argv) != 3 and len(sys.argv) != 4:
#    print("INVALID ARGS:")
#    print("audiosplit [AudiFile] [TimestampFile] (NameFile)")
#    exit()

name_file = None
tag_file = None
start_index = 1
OPTIONS = "n:t:"
try:
    opts, args = getopt.getopt(sys.argv[3:], "n:t:i:")
except getopt.GetoptError:
    print("audiosplitter.py [AudioFile] -n namefile -t tagfile")
    exit(1)
for opt, arg in opts:
    if opt == '-n':
        name_file = arg
    elif opt == '-t':
        tag_file = arg
    elif opt == '-i':
        start_index = int(arg)
audio_file = sys.argv[1]
timestamp_file = sys.argv[2]


TIMES = []

with open(timestamp_file) as rp:
    for x in rp:
        TIMES.append(x[:-1])
names = None
if name_file:
    names = []
    with open(name_file) as rp:
        for x in rp:
            names.append(x.strip())
        

full_file = AudioSegment.from_mp3(audio_file)

prev = 0
name = audio_file.split("/")[-1][:-4]
base_tags = {}
if tag_file:
    base_tags = getTags(tag_file)
    print(base_tags)
    #exit()
for i in range(len(TIMES)):
    cur = toMili(TIMES[i])
    part = full_file[prev:cur]
    prev = cur
    tag = base_tags
    tag["track"] = i + start_index
    if(names):
        tag["title"] = names[i]
        part.export("output/%s.mp3" %(names[i]), format="mp3",tags=tag)
        
        print("output/%s.mp3" %(names[i]))
    else:
        part.export("output/%s_%d.mp3" %(name,i), format="mp3",tags=tag)
        print("output/%s_%d.mp3" %(name,i))

part = full_file[prev:]
i = len(TIMES) 
tag = base_tags
tag["track"] = i + start_index 
if(names):
    tag["title"] = names[i]
    part.export("output/%s.mp3" %(names[i]), format="mp3", tags=tag)
    print("output/%s.mp3" %(names[i]))
else:
    part.export("output/%s_%d.mp3" %(name,i), format="mp3", tags=tag)
    print("output/%s_%d.mp3" %(name,i))

